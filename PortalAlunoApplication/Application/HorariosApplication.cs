﻿using Newtonsoft.Json;
using PortalAlunoApplication.Apoio;
using PortalAlunoModel.Requisicao;
using PortalAlunoModel.Retorno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PortalAlunoApplication.Application
{
    public class HorariosApplication
    {
        public RetornoHorario ListarHorario(String ideUsuario, String senha, String PeriodoLetivo)
        {

            RetornoHorario retorno = new RetornoHorario();


            try
            {


                Autenticacao autenticacao = new Autenticacao();
                autenticacao.Matricula = ideUsuario.Trim();
                autenticacao.Senha = senha.Trim();

                AutenticacaoPeriodo autenticacaoPeriodo = new AutenticacaoPeriodo();
                autenticacaoPeriodo.Autenticacao = autenticacao;
                autenticacaoPeriodo.PeriodoLetivo = PeriodoLetivo;

                var json = JsonConvert.SerializeObject(autenticacaoPeriodo);

                WebProxy proxy = new WebProxy(DadosProxy.URLProxy);
                proxy.Credentials = new NetworkCredential(DadosProxy.UsuarioProxy, DadosProxy.SenhaProxy);
                HttpClientHandler httpClientHandler = new HttpClientHandler()
                {
                    Proxy = proxy,
                    PreAuthenticate = true,
                    UseDefaultCredentials = false,
                };

                HttpClient client = new HttpClient();

                if (DadosProxy.UsarProxy == "S")
                {
                    client = new HttpClient(httpClientHandler);
                }

                client.MaxResponseContentBufferSize = 256000;

                var uri = new Uri("http://ws.unipam.edu.br/ServicoPortalAula/PortalAluno/ListarHorarios");

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                //content.Headers.Add("Expect100Continue", "false");

                var response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var content2 = response.Content.ReadAsStringAsync();
                    retorno = JsonConvert.DeserializeObject<RetornoHorario>(content2.Result);
                }


            }
            catch (Exception ex)
            {

                retorno.Retorno.Mensagem = ex.Message;
            }
            return retorno;


        }
    }
}
