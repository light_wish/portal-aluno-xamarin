﻿using Newtonsoft.Json;
using PortalAlunoApplication.Apoio;
using PortalAlunoModel.Modelo;
using PortalAlunoModel.Requisicao;
using PortalAlunoModel.Retorno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoApplication.Application
{
    public class AutenticarApplication
    {
        public Usuario Autenticar(String ideUsuario, String senha)
        {

            Usuario usuario = new Usuario();
            usuario.Autenticado = "N";
            usuario.Mensagem = "";
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {

                if (String.IsNullOrEmpty(ideUsuario))
                {
                    usuario.Mensagem = "Usuário não informado";
                    return usuario;
                }

                if (String.IsNullOrEmpty(senha))
                {
                    usuario.Mensagem = "Senha não informada";
                    return usuario;
                }

                Autenticacao autenticacao = new Autenticacao();
                Retorno retorno = new Retorno();

                autenticacao.Matricula = ideUsuario.Trim();
                autenticacao.Senha = senha.Trim();

                var json = JsonConvert.SerializeObject(autenticacao);

                WebProxy proxy = new WebProxy(DadosProxy.URLProxy);
                proxy.Credentials = new NetworkCredential(DadosProxy.UsuarioProxy,
                     DadosProxy.SenhaProxy);
                HttpClientHandler httpClientHandler = new HttpClientHandler()
                {
                    Proxy = proxy,
                    PreAuthenticate = true,
                    UseDefaultCredentials = false,
                };

                HttpClient client = new HttpClient();

                if (DadosProxy.UsarProxy == "S")
                {
                    client = new HttpClient(httpClientHandler);
                }

                client.MaxResponseContentBufferSize = 256000;

                var uri = new Uri("http://ws.unipam.edu.br/ServicoPortalAula/PortalAluno/Autenticar");

                //var uri = new Uri("http://201.16.249.85/ServicoPortalAula/PortalAluno/Autenticar");



                var content = new StringContent(json, Encoding.UTF8, "application/json");

                //content.Headers.Add("Expect100Continue", "false");

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var content2 = response.Content.ReadAsStringAsync();
                    retorno = JsonConvert.DeserializeObject<Retorno>(content2.Result);
                }

                usuario.Autenticado = retorno.Autenticado;
                usuario.Identificacao = retorno.Matricula;
                usuario.Nome = retorno.Nome;
                usuario.Mensagem = retorno.Mensagem;
            }

            //catch (HttpRequestException  exception)
            //{
            //    usuario.Mensagem = exception.Message;
            //}
            //catch (InvalidOperationException exception)
            //{
            //    usuario.Mensagem = exception.Message;
            //}
            catch (OperationCanceledException ex)
            {

                usuario.Mensagem = ex.Message;
            }
            catch (Exception ex)
            {

                usuario.Mensagem = ex.Message;
            }
            return usuario;


        }
    }
}
