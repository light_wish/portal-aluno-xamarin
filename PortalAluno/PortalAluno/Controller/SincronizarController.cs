﻿using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoApplication.Application;
using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Controller
{
    public class SincronizarController
    {

        public string SincronizarDados(string codigoPeriodoLetivo)
        {
            string mensagemRetorno = "";
            try
            {
                MBUsuarioDAL mbUsuarioDal = new MBUsuarioDAL();
                MBUsuario mbUsuario = mbUsuarioDal.Get();

                PortalAlunoApplication.Apoio.DadosProxy.UsarProxy = App.UsarProxy;
                PortalAlunoApplication.Apoio.DadosProxy.URLProxy = "proxy.unipam.edu.br:3128";
                PortalAlunoApplication.Apoio.DadosProxy.UsuarioProxy = App.UserProxy;
                PortalAlunoApplication.Apoio.DadosProxy.SenhaProxy = App.PassProxy;

                SincronizarApplication sinApp = new SincronizarApplication();

                var retorno = sinApp.SinronizarTudo(mbUsuario.Identificacao, mbUsuario.Senha, codigoPeriodoLetivo);
                if (retorno.Retorno.Mensagem.Trim() != "")
                    return retorno.Retorno.Mensagem;

                mensagemRetorno = CarregarDisciplinas(retorno.Disciplinas);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;

                mensagemRetorno = CarregarAulas(retorno.Aulas);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;

                mensagemRetorno = CarregarNotas(retorno.Notas);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;

                mensagemRetorno = CarregarAtividades(retorno.Atividades);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;
        }

        private string CarregarDisciplinas(List<Disciplina> Disciplinas)
        {
            string mensagemRetorno = "";
            try
            {
                MBDisciplina mbDisciplina;
                MBDisciplinaDAL mbDisciplinaDal = new Dal.MBDisciplinaDAL();
                mbDisciplinaDal.DeleteAll();
                foreach (var disciplina in Disciplinas)
                {
                    mbDisciplina = new MBDisciplina();
                    mbDisciplina.CodigoDisciplina = (short)disciplina.CodigoDisciplina;
                    mbDisciplina.CodigoTurma = disciplina.CodigoTurma;
                    mbDisciplina.NomeDisciplina = disciplina.NomeDisciplina;
                    mbDisciplinaDal.Add(mbDisciplina);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }

        private string CarregarAulas(List<Aula> Aulas)
        {
            string mensagemRetorno = "";
            try
            {
                MBAula mbAula;
                MBAulaDAL mbAulaDal = new MBAulaDAL();
                mbAulaDal.DeleteAll();
                foreach (var aula in Aulas)
                {
                    mbAula = new MBAula();
                    mbAula.CodigoDisciplina = (short)aula.CodigoDisciplina;
                    mbAula.CodigoAula = (short)aula.CodigoAula;
                    mbAula.Conteudo = aula.Conteudo;
                    mbAula.DataAula = Convert.ToDateTime(aula.DataAula);
                    mbAula.IndicadorFalta = aula.IndicadorDeFalta;
                    mbAula.QuantidadeAulas = (short)aula.QuantidadeAulas;

                    mbAulaDal.Add(mbAula);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }

        private string CarregarNotas(List<Nota> Notas)
        {
            string mensagemRetorno = "";
            try
            {
                MBNota mbNota;
                MBNotaDAL mbNotaDal = new MBNotaDAL();
                mbNotaDal.DeleteAll();
                foreach (var nota in Notas)
                {
                    mbNota = new MBNota();
                    mbNota.CodigoDisciplina = (short)nota.CodigoDisciplina;
                    mbNota.CodigoNota = (short)nota.CodigoNota;
                    mbNota.NomeAvaliacao = nota.NomeAvaliacao;
                    mbNota.ValorAluno = nota.ValorAluno == "" ? 0 : Convert.ToDecimal(nota.ValorAluno);
                    mbNota.ValorAvaliacao = nota.ValorAvaliacao == "" ? 0 : Convert.ToDecimal(nota.ValorAvaliacao);

                    mbNotaDal.Add(mbNota);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }

        private string CarregarAtividades(List<Atividade> Atividades)
        {
            string mensagemRetorno = "";
            try
            {
                MBAtividade mbAtividade;
                MBAtividadeDAL mbAtividadeDal = new MBAtividadeDAL();
                mbAtividadeDal.DeleteAll();
                foreach (var atividade in Atividades)
                {
                    mbAtividade = new MBAtividade();
                    mbAtividade.CodigoDisciplina = (short)atividade.CodigoDisciplina;
                    mbAtividade.CodigoAtividade = (short)atividade.CodigoAtividade;
                    mbAtividade.DataFim = Convert.ToDateTime(atividade.DataFim);
                    mbAtividade.DataInicio = Convert.ToDateTime(atividade.DataInicio);
                    mbAtividade.DescricaoAtividade = atividade.DescricaoAtividade;
                    mbAtividade.HoraFim = Convert.ToDateTime(atividade.HoraFim);
                    mbAtividade.IndicadorParticipacao = atividade.IndicadorParticipacao;

                    mbAtividadeDal.Add(mbAtividade);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }
    }
}
