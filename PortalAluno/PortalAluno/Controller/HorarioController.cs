﻿using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoApplication.Application;
using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Controller
{
    public class HorarioController
    {

        public string BuscarHorarios(string codigoPeriodoLetivo)
        {
            string mensagemRetorno = "";
            try
            {
                MBUsuarioDAL mbUsuarioDal = new MBUsuarioDAL();
                MBUsuario mbUsuario = mbUsuarioDal.Get();

                PortalAlunoApplication.Apoio.DadosProxy.UsarProxy = App.UsarProxy;
                PortalAlunoApplication.Apoio.DadosProxy.URLProxy = "proxy.unipam.edu.br:3128";
                PortalAlunoApplication.Apoio.DadosProxy.UsuarioProxy = App.UserProxy;
                PortalAlunoApplication.Apoio.DadosProxy.SenhaProxy = App.PassProxy;

                HorariosApplication horApp = new HorariosApplication();

                var retorno = horApp.ListarHorario(mbUsuario.Identificacao, mbUsuario.Senha, codigoPeriodoLetivo);
                if (retorno.Retorno.Mensagem.Trim() != "")
                    return retorno.Retorno.Mensagem;

                mensagemRetorno = CarregarHorarios(retorno.Horarios);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;
        }

        private string CarregarHorarios(List<Horario> Horarios)
        {
            string mensagemRetorno = "";
            try
            {
                MBHorario mbHorario;
                MBHorarioDAL mbHorarioDAL = new MBHorarioDAL();
                mbHorarioDAL.DeleteAll();
                foreach (var horario in Horarios)
                {
                    mbHorario = new MBHorario();
                    mbHorario.CodigoPeriodoLetivo = horario.CodigoPeriodoLetivo;
                    mbHorario.DescricaoDiaSemana = horario.DescricaoDiaSemana;
                    mbHorario.HoraFim = horario.HoraFim;
                    mbHorario.HoraInicio = horario.HoraInicio;
                    mbHorario.NomeDisciplina = horario.NomeDisciplina;
                    mbHorario.NumeroDiaDaSemana = horario.NumeroDiaDaSemana;

                    mbHorarioDAL.Add(mbHorario);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }

    }
}
