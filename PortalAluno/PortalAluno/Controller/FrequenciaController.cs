﻿//- Fazer tela de frequencia
//- Fazer calculo, mostrando porcetagem de falta, de acordo com a quantidade de aula
//- Usar a classe MBAula
//- Para calcular a quantidade Total de aula, somente multiplicar...

//Tabela é pra ser criado como:

//NomeDisciplina | QntAula | QntFalta | % Falta

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoApplication.Application;
using PortalAlunoModel.Modelo;

namespace PortalAluno.Controller
{
    public class FrequenciaController
    {
        MBDisciplinaDAL disciplinaDAL = new MBDisciplinaDAL();
        MBAulaDAL aulaDAL = new MBAulaDAL();
        FrequenciaDAL frequenciaDAL = new FrequenciaDAL();
        
        IEnumerable<MBAula> aulas { get; set; }
        IEnumerable<MBDisciplina> disciplinas { get; set; }

        public FrequenciaController()
        {
            aulas = aulaDAL.GetAll();
            disciplinas = disciplinaDAL.GetAll();
        }

        public void GravarNomeDisciplina()
        {
            frequenciaDAL.DeleteAll();

            Int16 index = 0;

            foreach (var aula in disciplinas)
            {
                var aulaPorDisciplina = aulaDAL.GetByDisciplina(aula.CodigoDisciplina);
                var quantidadeAula = 0;
                Int16 quantidadeFalta = 0;
                float porcetagemFalta = 0;

                foreach (var aulaDisciplina in aulaPorDisciplina)
                {
                    quantidadeAula += aulaDisciplina.QuantidadeAulas;

                    if (aulaDisciplina.IndicadorFalta == "S")
                    {
                        quantidadeFalta++;
                    }
                }

                var nomeDisciplina = disciplinaDAL.GetItemById(aula.CodigoDisciplina).NomeDisciplina;
                porcetagemFalta = (quantidadeFalta * 100) / quantidadeAula;
                porcetagemFalta.ToString();

                Frequencia frequencia;
                frequencia = new Frequencia();

                frequencia.CodigoFrequencia = index;
                frequencia.NomeDisciplina = nomeDisciplina;
                frequencia.NumeroFalta += quantidadeFalta;
                frequencia.QuantidadeAula += quantidadeAula;
                frequencia.PorcetagemFalta = porcetagemFalta + "%";

                frequenciaDAL.Add(frequencia);

                index++;
            }
        }
    }
}
