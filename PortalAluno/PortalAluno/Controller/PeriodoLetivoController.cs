﻿using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoApplication.Application;
using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Controller
{
    public class PeriodoLetivoController
    {
        public string BuscarPeriodosLetivos()
        {
            string mensagemRetorno = "";
            try
            {
                MBUsuarioDAL mbUsuarioDal = new MBUsuarioDAL();
                MBUsuario mbUsuario = mbUsuarioDal.Get();

                PortalAlunoApplication.Apoio.DadosProxy.UsarProxy = App.UsarProxy;
                PortalAlunoApplication.Apoio.DadosProxy.URLProxy = "proxy.unipam.edu.br:3128";
                PortalAlunoApplication.Apoio.DadosProxy.UsuarioProxy = App.UserProxy;
                PortalAlunoApplication.Apoio.DadosProxy.SenhaProxy = App.PassProxy;

                PeriodoLetivoApplication perApp = new PeriodoLetivoApplication();

                var retorno = perApp.ListarPeriodoLetivo(mbUsuario.Identificacao, mbUsuario.Senha);
                if (retorno.Retorno.Mensagem.Trim() != "")
                    return retorno.Retorno.Mensagem;

                mensagemRetorno = CarregarPeriodoLetivo(retorno.PeriodosLetivos);
                if (mensagemRetorno.Trim() != "")
                    return mensagemRetorno;


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;
        }

        private string CarregarPeriodoLetivo(List<PeriodoLetivo> PeriodosLetivos)
        {
            string mensagemRetorno = "";
            try
            {
                MBPeriodoLetivo mbPeriodoLetivo;
                MBPeriodoLetivoDAL mbPeriodoLetivoDAL = new MBPeriodoLetivoDAL();
                mbPeriodoLetivoDAL.DeleteAll();
                foreach (var periodoLetivo in PeriodosLetivos)
                {
                    mbPeriodoLetivo = new MBPeriodoLetivo();
                    mbPeriodoLetivo.CodigoPeriodoLetivo = periodoLetivo.CodigoPeriodoLetivo;
                    mbPeriodoLetivo.DescricaoPeriodoLetivo = periodoLetivo.DescricaoPeriodoLetivo;

                    mbPeriodoLetivoDAL.Add(mbPeriodoLetivo);

                }


            }
            catch (Exception ex)
            {
                mensagemRetorno = ex.Message;
            }
            return mensagemRetorno;

        }

    }
}
