﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using PortalAluno.Paginas.PaginasMenu;
using PortalAluno.Infraestrutura;
using PortalAluno.Paginas.AutenticacaoPaginas;

namespace PortalAluno
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.Master = new MenuMaster();
            this.Detail = new NavigationPage(new MenuDetail());
            App.MasterDetail = this;

            App.PaginaAtual = "MainPage";
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();

            if (App.PaginaAtual == "MainPage")
            {
                if (Device.OS == TargetPlatform.Android)
                {
                    DependencyService.Get<IFecharSistema>().FecharAplicativo();
                }
            }

            App.PaginaAtual = "MainPage";

            return true;
        }

        protected override void OnAppearing()
        {
            // when showing this window, if the Login hasn't been done, show
            // the login screen...

           App.PeriodoAtual = "1/2017";

            if (!App.isLogged)
            {
                Navigation.PushModalAsync(new NavigationPage(new Autenticacao()));
            }
            base.OnAppearing();
        }
    }
}
