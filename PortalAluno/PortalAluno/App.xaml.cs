﻿using PortalAluno.Paginas.AutenticacaoPaginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using PortalAluno.Paginas.PaginasMenu;
using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoModel.Modelo;
using PortalAlunoApplication.Application;
using PortalAluno.Controller;

namespace PortalAluno
{
    public partial class App : Application
    {
        public static MasterDetailPage MasterDetail { get; set; }
        public static string PaginaAtual { get; set; }
        public static bool isLogged { get; set; }
        public static string PeriodoAtual { get; set; }
        private static string user;
        private static string pass;

        public static string UsarProxy { get; set; }
        public static string UserProxy { get; set; }
        public static string PassProxy { get; set; }

        Usuario usuario = new Usuario();
        AutenticarApplication autApp = new AutenticarApplication();

        public async static Task NavegarPaginaMasterDetail(Page pagina)
        {
            MasterDetail.IsPresented = false;
            //await MasterDetail.Detail.Navigation.PushAsync(pagina);
            MasterDetail.Detail = new NavigationPage(pagina);
        }

        private void CriarTabelas()
        {
            BancoDeDadosDAL bancoDeDados = new BancoDeDadosDAL();
            bancoDeDados.CriarTabelas();
        }

        private void CriarUsuario(string Nome)
        {
            MBUsuarioDAL mbUsuarioDAL = new MBUsuarioDAL();

            MBUsuario mbUsuario = mbUsuarioDAL.Get();
            if (mbUsuario == null)
            {
                mbUsuario = new MBUsuario();
                mbUsuario.Identificacao = user;
                mbUsuario.Senha = pass;
                mbUsuario.Nome = Nome;
                mbUsuarioDAL.Add(mbUsuario);
            }
            else
            {
                mbUsuario.Identificacao = user;
                mbUsuario.Senha = pass;
                mbUsuario.Nome = Nome;
                mbUsuarioDAL.Update(mbUsuario);
            }
        }

        public App()
        {
            InitializeComponent();
            UsarProxy = "N";
            UserProxy = "a14034434";
            PassProxy = "74376784";

            user = "14034434";
            pass = "mmKnp698";
            PeriodoAtual = "1/2017";
            usuario = autApp.Autenticar(user, pass);
            CriarTabelas();
            CriarUsuario(usuario.Nome);
            SincronizarController sincronizar = new SincronizarController();
            sincronizar.SincronizarDados(PeriodoAtual);
            //MainPage = new NavigationPage(new MainPage());
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
