﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBDisciplina
    {
        [PrimaryKey]
        public Int16 CodigoDisciplina { get; set; }
        [MaxLength(120)  Collation("NOCASE") NotNull]
        public string NomeDisciplina { get; set; }
        [MaxLength(20)  Collation("NOCASE") NotNull]
        public string CodigoTurma { get; set; }

        [OneToMany]
        public List<MBNota> Notas { get; set; }

        [OneToMany]
        public List<MBAtividade> Atividades { get; set; }

        [OneToMany]
        public List<MBAula> Aulas { get; set; }

    }
}
