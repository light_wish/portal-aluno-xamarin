﻿ 
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class SituacaoAlerta
    {
        [PrimaryKey, AutoIncrement]
        public int? IdSituacao { get; set; }
        public string Descricao { get; set; }

        [OneToMany]
        public List<Alerta> Alertas { get; set; }

        public override bool Equals(object obj)
        {
            SituacaoAlerta situacaoEvento = obj as SituacaoAlerta;
            if (situacaoEvento == null)
            {
                return false;
            }
            return (IdSituacao.Equals(situacaoEvento.IdSituacao));
        }
        public override int GetHashCode()
        {
            return IdSituacao.GetHashCode();
        }
    }
}