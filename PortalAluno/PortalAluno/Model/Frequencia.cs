﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Frequencia
    {
        [PrimaryKey]
        public Int16 CodigoFrequencia { get; set; }

        public Int16 NumeroFalta { get; set; }

        public Int32 QuantidadeAula { get; set; }

        [Collation("NOCASE")]
        public string NomeDisciplina { get; set; }

        [Collation("NOCASE")]
        public string PorcetagemFalta { get; set; }
    }
}
