﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBPeriodoLetivo
    {
        [PrimaryKey]
        public string CodigoPeriodoLetivo { get; set; }
        [MaxLength(100)  Collation("NOCASE") NotNull]
        public string DescricaoPeriodoLetivo { get; set; }
    }
}
