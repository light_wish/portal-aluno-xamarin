﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBAtividade
    {
        [PrimaryKey]
        public Int16 CodigoAtividade { get; set; }
        [Collation("NOCASE") NotNull]
        public string DescricaoAtividade { get; set; }
        [NotNull]
        public DateTime DataInicio { get; set; }
        [NotNull]
        public DateTime DataFim { get; set; }
        [NotNull]
        public DateTime HoraFim { get; set; }
        [MaxLength(1)  Collation("NOCASE") NotNull]
        public string IndicadorParticipacao { get; set; }

        [ForeignKey(typeof(MBDisciplina)) NotNull]
        public Int16 CodigoDisciplina { get; set; }
    }
}
