﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBHorario
    {
        [PrimaryKey, AutoIncrement]
        public Int32 IdMBHorario { get; set; }
        [MaxLength(10)  Collation("NOCASE") NotNull]
        public string CodigoPeriodoLetivo { get; set; }
        [Collation("NOCASE") NotNull]
        public int NumeroDiaDaSemana { get; set; }
        [MaxLength(20)  Collation("NOCASE") NotNull]
        public string DescricaoDiaSemana { get; set; }
        [MaxLength(5)  Collation("NOCASE") NotNull]
        public string HoraInicio { get; set; }
        [MaxLength(5)  Collation("NOCASE") NotNull]
        public string HoraFim { get; set; }
        [MaxLength(100)  Collation("NOCASE") NotNull]
        public string NomeDisciplina { get; set; }
    }
}
