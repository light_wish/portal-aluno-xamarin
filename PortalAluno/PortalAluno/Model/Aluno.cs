﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Aluno
    {
        [PrimaryKey, AutoIncrement]
        public int? Matricula { get; set; }

        public string Nome { get; set; }

        [ForeignKey(typeof(Sexo))]
        public int IdSexo { get; set; }

        [ForeignKey(typeof(Cidade))]
        public int IdCidade { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public Sexo Sexo { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public Cidade Cidade { get; set; }

        public override bool Equals(object obj)
        {
            Aluno aluno = obj as Aluno;
            if (aluno == null)
            {
                return false;
            }
            return (IdCidade.Equals(aluno.Matricula));
        }

        public override int GetHashCode()
        {
            return Matricula.GetHashCode();
        }
    }
}
