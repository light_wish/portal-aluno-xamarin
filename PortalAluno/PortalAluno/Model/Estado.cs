﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Estado
    {
        [PrimaryKey, AutoIncrement]
        public int? IdEstado { get; set; }
        public string Descricao { get; set; }

        [OneToMany]
        public List<Cidade> Cidades { get; set; }

        public override bool Equals(object obj)
        {
            Estado estado = obj as Estado;
            if (estado == null)
            {
                return false;
            }
            return (IdEstado.Equals(estado.IdEstado));
        }

        public override int GetHashCode()
        {
            return IdEstado.GetHashCode();
        }
    }
}
