﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBUsuario
    {

        public string Identificacao { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }

    }
}
