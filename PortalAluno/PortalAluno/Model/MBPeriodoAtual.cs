﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBPeriodoAtual
    {
        [PrimaryKey]
        public string periodo { get; set; }
    }
}
