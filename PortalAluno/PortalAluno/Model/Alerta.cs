﻿ using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Alerta
    {

        [PrimaryKey, AutoIncrement]
        public int? IdAlerta { get; set; }
        public string DescricaoAlerta { get; set; }
        public DateTime Data { get; set; }
        public string Hora { get; set; }
        public double Valor { get; set; }


        [ForeignKey(typeof(SituacaoAlerta))]
        public int IdSituacao { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public SituacaoAlerta SituacaoAlerta { get; set; }

        public override bool Equals(object obj)
        {
            Alerta alerta = obj as Alerta;
            if (alerta == null)
            {
                return false;
            }
            return (IdAlerta.Equals(alerta.IdAlerta));
        }
        public override int GetHashCode()
        {
            return IdAlerta.GetHashCode();
        }
    }
}
