﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Cidade
    {
        [PrimaryKey, AutoIncrement]
        public int? IdCidade { get; set; }

        public string Nome { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }

        [ForeignKey(typeof(Estado))]
        public int IdEstado { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public Estado Estado { get; set; }

        public override bool Equals(object obj)
        {
            Cidade cidade = obj as Cidade;
            if (cidade == null)
            {
                return false;
            }
            return (IdCidade.Equals(cidade.IdCidade));
        }

        public override int GetHashCode()
        {
            return IdCidade.GetHashCode();
        }
    }
}
