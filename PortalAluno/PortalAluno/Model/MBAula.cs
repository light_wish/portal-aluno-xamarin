﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBAula
    {
        [PrimaryKey]
        public Int16 CodigoAula { get; set; }

        [NotNull]
        public DateTime DataAula { get; set; }
        [MaxLength(1)  Collation("NOCASE") NotNull]
        public string IndicadorFalta { get; set; }
        [Collation("NOCASE") NotNull]
        public string Conteudo { get; set; }
        [NotNull]
        public Int16 QuantidadeAulas { get; set; }
   
        public Int16 NumeroFalta { get; set; }

        [Collation("NOCASE")]
        public string NomeDisciplina { get; set; }

        [Collation("NOCASE")]
        public string PorcetagemFalta { get; set; }

        [ForeignKey(typeof(MBDisciplina)) NotNull]
        public Int16 CodigoDisciplina { get; set; }
    }
}
