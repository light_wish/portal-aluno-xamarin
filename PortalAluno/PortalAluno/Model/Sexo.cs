﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class Sexo
    {
        [PrimaryKey, AutoIncrement]
        public int? IdSexo { get; set; }
        public string Descricao { get; set; }

        public override bool Equals(object obj)
        {
            Sexo sexo = obj as Sexo;
            if (sexo == null)
            {
                return false;
            }
            return (IdSexo.Equals(sexo.IdSexo));
        }

        public override int GetHashCode()
        {
            return IdSexo.GetHashCode();
        }
    }
}
