﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Model
{
    public class MBNota
    {
        [PrimaryKey]
        public Int16 CodigoNota { get; set; }
        [NotNull]
        public decimal ValorAluno { get; set; }
        [NotNull]
        public decimal ValorAvaliacao { get; set; }
        [MaxLength(120)  Collation("NOCASE") NotNull]
        public string NomeAvaliacao { get; set; }

        [ForeignKey(typeof(MBDisciplina)) NotNull]
        public Int16 CodigoDisciplina { get; set; }

        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public MBDisciplina MBDisciplina { get; set; }
    }
}
