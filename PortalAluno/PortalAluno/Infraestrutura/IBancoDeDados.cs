﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Infraestrutura
{
    public interface IBancoDeDados
    {
        SQLiteConnection DbConnection();
    }
}
