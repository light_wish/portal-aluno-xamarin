﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Infraestrutura
{
    public interface IFecharSistema
    {
        void FecharAplicativo();
    }
}
