﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class AlertaDAL
    {

        private SQLiteConnection sqlConnection;
        public AlertaDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();

            this.sqlConnection.CreateTable<Alerta>();
        }

        public IEnumerable<Alerta> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<Alerta>() select t).OrderBy(i => i.Data).ToList();

        }
        public Alerta GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<Alerta>().FirstOrDefault(t => t.IdAlerta == id);

        }
        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Alerta>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(Alerta alerta)
        {
            try
            {
                sqlConnection.Insert(alerta);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(Alerta alerta)
        {
            try
            {
                sqlConnection.Update(alerta);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
