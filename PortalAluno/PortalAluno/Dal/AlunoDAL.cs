﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace PortalAluno.Dal
{
    public class AlunoDAL
    {
        private SQLiteConnection sqlConnection;

        public AlunoDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
            this.sqlConnection.CreateTable<Aluno>();
        }

        public IEnumerable<Aluno> Todos()
        {

            var alunos = (from t in sqlConnection.GetAllWithChildren<Aluno>() select t).OrderBy(i => i.Matricula).ToList();

            return alunos;
        }

        public Aluno GetItemById(int id)
        {

            var aluno = sqlConnection.GetAllWithChildren<Aluno>().FirstOrDefault(t => t.Matricula == id);

            return aluno;
        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Aluno>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Add(Aluno aluno)
        {
            try
            {
                sqlConnection.Insert(aluno);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Update(Aluno aluno)
        {
            try
            {
                sqlConnection.Update(aluno);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
