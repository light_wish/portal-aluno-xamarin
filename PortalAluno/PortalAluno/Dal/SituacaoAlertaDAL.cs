﻿ 
//using PortalAluno.Helper;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class SituacaoAlertaDAL
    {
        private SQLiteConnection sqlConnection;
        public SituacaoAlertaDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
            this.sqlConnection.CreateTable<SituacaoAlerta>();
        }

        public IEnumerable<SituacaoAlerta> GetAll()
        {

            var situacoesAlertas = (from t in sqlConnection.Table<SituacaoAlerta>() select t).OrderBy(i => i.Descricao).ToList();

            return situacoesAlertas;

            // return (from t in sqlConnection.Table<SituacaoAlerta>() select t).OrderBy(i => i.Descricao).ToList();
            // return sqlConnection.Query<SituacaoAlerta>("SELECT * FROM SituacaoAlerta Order by Descricao").AsEnumerable();

            //List<SituacaoAlerta> situacoes = new List<SituacaoAlerta>();
            //var command = sqlConnection.CreateCommand("SELECT * FROM SituacaoAlerta");
            //var query = command.ExecuteQuery<SituacaoAlerta>();
            //foreach (var q in query)
            //{
            //    situacoes.Add(q);
            //}

            //return situacoes;


        }
        public SituacaoAlerta GetItemById(int id)
        {

            var SituacaoAlerta = sqlConnection.Table<SituacaoAlerta>().FirstOrDefault(t => t.IdSituacao == id);

            return SituacaoAlerta;

            // return sqlConnection.Query<SituacaoAlerta>("SELECT * FROM SituacaoAlerta where IdSituacao = " + id).FirstOrDefault();


            //SituacaoAlerta situacaoAlerta = new SituacaoAlerta();
            //var command = sqlConnection.CreateCommand("SELECT * FROM SituacaoAlerta where IdSituacao = " + id);
            //var query = command.ExecuteQuery<SituacaoAlerta>();

            //situacaoAlerta = query.FirstOrDefault();

            //return situacaoAlerta;

        }
        public void DeleteById(int id)
        {
            try
            {
                //sqlConnection.Delete<SituacaoAlerta>(id);
                var command = sqlConnection.CreateCommand("Delete FROM SituacaoAlerta where IdSituacao = " + id);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(SituacaoAlerta situacaoAlerta)
        {
            try
            {
                //sqlConnection.Insert(situacaoAlerta);

                var command = sqlConnection.CreateCommand("insert into SituacaoAlerta (descricao) values ('" + situacaoAlerta.Descricao + "')");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(SituacaoAlerta situacaoAlerta)
        {
            try
            {
                //sqlConnection.Update(situacaoAlerta);

                var command = sqlConnection.CreateCommand("update SituacaoAlerta set descricao = '" + situacaoAlerta.Descricao + "' where IdSituacao = " + situacaoAlerta.IdSituacao);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
