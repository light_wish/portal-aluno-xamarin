﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;

namespace PortalAluno.Dal
{
    public class EstadoDAL
    {
        private SQLiteConnection sqlConnection;

        public EstadoDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
            this.sqlConnection.CreateTable<Estado>();
        }

        public IEnumerable<Estado> Todos()
        {

            var estados = (from t in sqlConnection.Table<Estado>() select t).OrderBy(i => i.Descricao).ToList();

            return estados;
        }

        public Estado GetItemById(int id)
        {

            var estado = sqlConnection.Table<Estado>().FirstOrDefault(t => t.IdEstado == id);

            return estado;
        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Estado>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Add(Estado estado)
        {
            try
            {
                sqlConnection.Insert(estado);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Update(Estado estado)
        {
            try
            {
                sqlConnection.Update(estado);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
