﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBAtividadeDAL
    {

        private SQLiteConnection sqlConnection;
        public MBAtividadeDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();
        }

        public IEnumerable<MBAtividade> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAtividade>()
                    select t).OrderBy(i => i.DataInicio).ToList();

        }

        public IEnumerable<MBAtividade> GetByParticipacao()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAtividade>()
                    select t).Where(t => t.IndicadorParticipacao == "N").OrderBy(i => i.DataInicio).ToList();

        }

        public IEnumerable<MBAtividade> GetByData()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAtividade>()
                    select t).Where(t => t.DataFim >= DateTime.Now).ToList();

        }

        public IEnumerable<MBAtividade> GetByDisciplina(int id)
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAtividade>()
                    select t).Where(t => t.CodigoDisciplina == id)
                    .OrderBy(i => i.DataInicio).ToList();

        }
        public MBAtividade GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<MBAtividade>().FirstOrDefault(t => t.CodigoAtividade == id);

        }

        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBAtividade ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<MBAtividade>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBAtividade mbAtividade)
        {
            try
            {
                sqlConnection.Insert(mbAtividade);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBAtividade mbAtividade)
        {
            try
            {
                sqlConnection.Update(mbAtividade);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
