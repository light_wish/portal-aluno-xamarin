﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class BancoDeDadosDAL
    {
        SQLiteConnection sqlConnection;

        public BancoDeDadosDAL()
        {
            sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
        }

        public SQLiteConnection GetConexao()
        {
            return sqlConnection;
        }

        public void CriarTabelas()
        {

            try
            {
                var command = sqlConnection.CreateCommand(TabelaMBDisciplina());
                command.ExecuteNonQuery();
                command = sqlConnection.CreateCommand(TabelaMBAula());
                command.ExecuteNonQuery();
                command = sqlConnection.CreateCommand(TabelaMBAtividade());
                command.ExecuteNonQuery();
                command = sqlConnection.CreateCommand(MBNota());
                command.ExecuteNonQuery();

                command = sqlConnection.CreateCommand(TabelaUsuario());
                command.ExecuteNonQuery();

                command = sqlConnection.CreateCommand(TabelaMBAulaIndice());
                command.ExecuteNonQuery();
                command = sqlConnection.CreateCommand(TabelaMBAtividadeIndice());
                command.ExecuteNonQuery();
                command = sqlConnection.CreateCommand(MBNotaIndice());
                command.ExecuteNonQuery();

                command = sqlConnection.CreateCommand(TabelaPeriodoLetivo());
                command.ExecuteNonQuery();

                command = sqlConnection.CreateCommand(TabelaHorario());
                command.ExecuteNonQuery();

                sqlConnection.CreateTable<MBPeriodoAtual>();
                sqlConnection.CreateTable<Frequencia>();

                sqlConnection.CreateTable<SituacaoAlerta>();
                sqlConnection.CreateTable<Alerta>();

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
                sqlConnection.Close();
                sqlConnection.Dispose();
            }

        }


        private string TabelaHorario()
        {
            string comando = "";

            comando = "CREATE TABLE IF NOT EXISTS MBHorario( " +
                "IdMBHorario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "CodigoPeriodoLetivo varchar (10) not null," +
                "NumeroDiaDaSemana INTEGER not null," +
                "DescricaoDiaSemana varchar (20) not null," +
                "HoraInicio varchar (5) not null," +
                "HoraFim varchar (5) not null," +
                "NomeDisciplina varchar (100) not null);";

            return comando;
        }

        private string TabelaPeriodoLetivo()
        {
            string comando = "";

            comando = "CREATE TABLE IF NOT EXISTS MBPeriodoLetivo( " +
                "CodigoPeriodoLetivo varchar(10) primary key not null ," +
                "DescricaoPeriodoLetivo varchar (100) not null);";

            return comando;
        }

        private string TabelaUsuario()
        {
            string comando = "";

            comando = "CREATE TABLE IF NOT EXISTS MBUsuario( " +
                "Identificacao varchar(20) primary key not null ," +
                "Nome varchar (120) not null," +
                "Senha varchar(200) not null);";

            return comando;
        }

        private string TabelaMBDisciplina()
        {

            string comando = "";

            comando = "CREATE TABLE IF NOT EXISTS MBDisciplina( " +
                "CodigoDisciplina smallint primary key not null ," +
                "NomeDisciplina varchar (200) not null," +
                "CodigoTurma varchar(20) not null);";

            return comando;

        }
        private string TabelaMBAula()
        {
            string comando = "";
            comando = " CREATE TABLE IF NOT EXISTS MBAula( " +
            "   CodigoAula smallint primary key not null, " +
            "   DataAula Date not null, " +
            "   IndicadorFalta char(1) not null, " +
            "   Conteudo varchar collate NOCASE not null, " +
            "   QuantidadeAulas smallint not null, " +
            "   NumeroFalta smallint not null, " +
            "   CodigoDisciplina smallint not null, " +
            "   NomeDisciplina varchar (200)," +
            "   PorcetagemFalta varchar (200)," +
            "  foreign key(CodigoDisciplina) references MBDisciplina(CodigoDisciplina) ); ";
            return comando;
        }

        private string TabelaMBAulaIndice()
        {
            string comando = "";
            comando = "create index if not exists IX_MBAula_Disciplina on MBAula (CodigoDisciplina); ";
            return comando;
        }

        private string TabelaMBAtividade()
        {
            string comando = "";
            comando = " CREATE TABLE IF NOT EXISTS MBAtividade(" +
                 " CodigoAtividade smallint primary key not null ," +
                 " DescricaoAtividade varchar not null collate NOCASE," +
                 " DataInicio Date not null ," +
                 " DataFim Date not null ," +
                 " HoraFim string not null ," +
                 " IndicadorParticipacao varchar(1) not null collate NOCASE," +
                 " CodigoDisciplina smallint not null," +
                 " foreign key(CodigoDisciplina) references MBDisciplina(CodigoDisciplina) );";
            return comando;
        }

        private string TabelaMBAtividadeIndice()
        {
            string comando = "";
            comando = "create index if not exists IX_MBAtividade_Disciplina on MBAtividade (CodigoDisciplina);";
            return comando;
        }
        private string MBNota()
        {
            string comando = "";
            comando = "CREATE TABLE IF NOT EXISTS MBNota(" +
            "     CodigoNota smallint primary key not null ," +
            "     ValorAluno  float not null ," +
            "     ValorAvaliacao  float not null ," +
            "     NomeAvaliacao varchar(120) not null collate NOCASE," +
            "     CodigoDisciplina  smallint not null," +
            "      foreign key(CodigoDisciplina) references MBDisciplina(CodigoDisciplina)" +
            "    );";
            return comando;
        }

        private string MBNotaIndice()
        {
            string comando = "";
            comando = "create index if not exists IX_MBNota_Disciplina on MBNota(CodigoDisciplina); ";
            return comando;
        }
    }
}
