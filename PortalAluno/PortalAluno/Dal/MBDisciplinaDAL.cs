﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBDisciplinaDAL
    {

        private SQLiteConnection sqlConnection;
        public MBDisciplinaDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<MBDisciplina> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBDisciplina>()
                    select t).OrderBy(i => i.NomeDisciplina).ToList();

        }
        public MBDisciplina GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<MBDisciplina>().FirstOrDefault(t => t.CodigoDisciplina == id);

        }

        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBDisciplina ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<MBDisciplina>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBDisciplina mbDisciplina)
        {
            try
            {
                sqlConnection.Insert(mbDisciplina);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBDisciplina mbDisciplina)
        {
            try
            {
                sqlConnection.Update(mbDisciplina);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
