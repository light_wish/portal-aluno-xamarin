﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace PortalAluno.Dal
{
    public class MBPeriodoAtualDAL
    {
        private SQLiteConnection sqlConnection;

        public MBPeriodoAtualDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();
        }

        public MBPeriodoAtual Get()
        {
            return sqlConnection.GetAllWithChildren<MBPeriodoAtual>().FirstOrDefault();
        }

        public void Add(MBPeriodoAtual periodoAtual)
        {
            try
            {
                sqlConnection.Insert(periodoAtual);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }
        }

        public void Update(MBPeriodoAtual periodoAtual)
        {
            try
            {
                sqlConnection.Update(periodoAtual);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
