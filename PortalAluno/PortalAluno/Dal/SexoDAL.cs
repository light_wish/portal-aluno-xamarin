﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;

namespace PortalAluno.Dal
{
    public class SexoDAL
    {
        private SQLiteConnection sqlConnection;

        public SexoDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
            this.sqlConnection.CreateTable<Sexo>();
        }

        public IEnumerable<Sexo> Todos()
        {

            var generosSexual = (from t in sqlConnection.Table<Sexo>() select t).OrderBy(i => i.Descricao).ToList();

            return generosSexual;
        }

        public Sexo GetItemById(int id)
        {

            var generoSexual = sqlConnection.Table<Sexo>().FirstOrDefault(t => t.IdSexo == id);

            return generoSexual;
        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Sexo>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Add(Sexo sexo)
        {
            try
            {
                sqlConnection.Insert(sexo);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Update(Sexo sexo)
        {
            try
            {
                sqlConnection.Update(sexo);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
