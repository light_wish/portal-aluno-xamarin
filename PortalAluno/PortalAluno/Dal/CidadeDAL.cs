﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace PortalAluno.Dal
{
    public class CidadeDAL
    {
        private SQLiteConnection sqlConnection;

        public CidadeDAL()
        {
            this.sqlConnection = Xamarin.Forms.DependencyService.Get<IBancoDeDados>().DbConnection();
            this.sqlConnection.CreateTable<Cidade>();
        }

        public IEnumerable<Cidade> Todos()
        {

            var cidades = (from t in sqlConnection.GetAllWithChildren<Cidade>() select t).OrderBy(i => i.IdCidade).ToList();

            return cidades;
        }

        public Cidade GetItemById(int id)
        {

            var cidade = sqlConnection.GetAllWithChildren<Cidade>().FirstOrDefault(t => t.IdCidade == id);

            return cidade;
        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Cidade>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Add(Cidade cidade)
        {
            try
            {
                sqlConnection.Insert(cidade);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void Update(Cidade cidade)
        {
            try
            {
                sqlConnection.Update(cidade);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
