﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBNotaDAL
    {

        private SQLiteConnection sqlConnection;
        public MBNotaDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<MBNota> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBNota>()
                    select t).OrderBy(i => i.NomeAvaliacao).ToList();

        }

        public IEnumerable<MBNota> GetByDisciplina(int id)
        {

            return (from t in sqlConnection.GetAllWithChildren<MBNota>()
                    select t).Where(t => t.CodigoDisciplina == id)
                    .OrderBy(i => i.NomeAvaliacao).ToList();

        }
        public MBNota GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<MBNota>().FirstOrDefault(t => t.CodigoNota == id);

        }

        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBNota ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<MBNota>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBNota mbNota)
        {
            try
            {
                sqlConnection.Insert(mbNota);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBNota mbNota)
        {
            try
            {
                sqlConnection.Update(mbNota);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
