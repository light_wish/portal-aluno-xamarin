﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBAulaDAL
    {

        private SQLiteConnection sqlConnection;
        public MBAulaDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<MBAula> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAula>()
                    select t).OrderBy(i => i.DataAula).ToList();

        }

        public IEnumerable<MBAula> GetByDisciplina(int id)
        {

            return (from t in sqlConnection.GetAllWithChildren<MBAula>()
                    select t).Where(t => t.CodigoDisciplina == id)
                    .OrderBy(i => i.DataAula).ToList();

        }
        public MBAula GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<MBAula>().FirstOrDefault(t => t.CodigoAula == id);

        }


        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBAula ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<MBAula>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBAula mbAula)
        {
            try
            {
                sqlConnection.Insert(mbAula);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBAula mbAula)
        {
            try
            {
                sqlConnection.Update(mbAula);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
