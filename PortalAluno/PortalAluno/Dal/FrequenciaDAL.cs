﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class FrequenciaDAL
    {

        private SQLiteConnection sqlConnection;
        public FrequenciaDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<Frequencia> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<Frequencia>()
                    select t).OrderBy(i => i.CodigoFrequencia).ToList();

        }

        public Frequencia GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<Frequencia>().FirstOrDefault(t => t.CodigoFrequencia == id);

        }


        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM Frequencia ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<Frequencia>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(Frequencia frenquencia)
        {
            try
            {
                sqlConnection.Insert(frenquencia);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(Frequencia frenquencia)
        {
            try
            {
                sqlConnection.Update(frenquencia);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
