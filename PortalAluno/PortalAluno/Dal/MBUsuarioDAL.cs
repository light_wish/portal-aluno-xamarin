﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBUsuarioDAL
    {

        private SQLiteConnection sqlConnection;
        public MBUsuarioDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public MBUsuario Get()
        {

            return (from t in sqlConnection.Table<MBUsuario>()
                    select t).FirstOrDefault();

        }

        public void Add(MBUsuario mbUsuario)
        {
            try
            {
                sqlConnection.Insert(mbUsuario);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBUsuario mbUsuario)
        {
            try
            {
                sqlConnection.Update(mbUsuario);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
