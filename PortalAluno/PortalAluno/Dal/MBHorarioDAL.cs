﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBHorarioDAL
    {

        private SQLiteConnection sqlConnection;
        public MBHorarioDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<MBHorario> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBHorario>()
                    select t).OrderBy(i => i.NumeroDiaDaSemana).ToList();

        }
        public MBHorario GetItemById(int id)
        {

            return sqlConnection.GetAllWithChildren<MBHorario>().FirstOrDefault(t => t.IdMBHorario == id);

        }

        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBHorario ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void DeleteById(int id)
        {
            try
            {
                sqlConnection.Delete<MBHorario>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBHorario mbHorario)
        {
            try
            {
                sqlConnection.Insert(mbHorario);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBHorario mbHorario)
        {
            try
            {
                sqlConnection.Update(mbHorario);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
