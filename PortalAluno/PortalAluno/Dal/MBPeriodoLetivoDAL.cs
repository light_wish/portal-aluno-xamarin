﻿using PortalAluno.Infraestrutura;
using PortalAluno.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAluno.Dal
{
    public class MBPeriodoLetivoDAL
    {

        private SQLiteConnection sqlConnection;
        public MBPeriodoLetivoDAL()
        {
            BancoDeDadosDAL BancoDeDadosDAL = new BancoDeDadosDAL();
            sqlConnection = BancoDeDadosDAL.GetConexao();

        }

        public IEnumerable<MBPeriodoLetivo> GetAll()
        {

            return (from t in sqlConnection.GetAllWithChildren<MBPeriodoLetivo>()
                    select t).OrderBy(i => i.CodigoPeriodoLetivo).ToList();

        }
        public MBPeriodoLetivo GetItemById(string id)
        {

            return sqlConnection.GetAllWithChildren<MBPeriodoLetivo>().FirstOrDefault(t => t.CodigoPeriodoLetivo == id);

        }

        public void DeleteAll()
        {
            try
            {
                var command = sqlConnection.
                    CreateCommand("Delete FROM MBPeriodoLetivo ");
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }

        public void DeleteById(string id)
        {
            try
            {
                sqlConnection.Delete<MBPeriodoLetivo>(id);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Add(MBPeriodoLetivo mbPeriodoLetivo)
        {
            try
            {
                sqlConnection.Insert(mbPeriodoLetivo);

            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
        public void Update(MBPeriodoLetivo mbPeriodoLetivo)
        {
            try
            {
                sqlConnection.Update(mbPeriodoLetivo);
            }
            catch (Exception ex)
            {
                var mensagemErro = ex.Message;
            }

        }
    }
}
