﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Paginas.PaginasMenu;

namespace PortalAluno.Apoio
{
    public class GerarMenu
    {
        public List<Menu> GerarMenuAplicativo()
        {
            List<Menu> listaMenu = new List<Apoio.Menu>();

            //var pagina = new Menu() { Title = "Inicio", Icon = "icon.png", TargetType = typeof(MenuDetail) };
            //listaMenu.Add(pagina);

            var pagina = new Menu() { Title = "Página Um", Icon = "barcode.png"};
            listaMenu.Add(pagina);

            pagina = new Menu() { Title = "Página Dois", Icon = "key.png"};
            listaMenu.Add(pagina);

            return listaMenu;
        }
    }
}
