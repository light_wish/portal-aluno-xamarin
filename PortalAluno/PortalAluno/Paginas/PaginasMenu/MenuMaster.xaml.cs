﻿using PortalAluno.Controller;
using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAluno.Paginas.AutenticacaoPaginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.PaginasMenu
{
    public partial class MenuMaster : ContentPage
    {
        private MBUsuarioDAL usuarioDAL = new MBUsuarioDAL();
        private List<MBPeriodoLetivo> ListaPeriodo;
        private PeriodoLetivoController periodoController = new PeriodoLetivoController();

        public MenuMaster()
        {
            InitializeComponent();
            //lblNomeAluno.Text = usuarioDAL.Get().Nome;
            //lblMatricula.Text = usuarioDAL.Get().Identificacao;
            periodoController.BuscarPeriodosLetivos();
            pickerPeriodo.SelectedIndexChanged += this.selectedValuePicker;
            Inicializar();
        }
        void btnSincronizar(object sender, EventArgs e)
        {
            SincronizarController sincronizar = new SincronizarController();
            sincronizar.SincronizarDados(App.PeriodoAtual);
        }

        private void Inicializar()
        {
            pickerPeriodo.Items.Clear();
            ListaPeriodo = new List<MBPeriodoLetivo>();

            MBPeriodoLetivoDAL dalPeriodo = new MBPeriodoLetivoDAL();

            var periodos = dalPeriodo.GetAll();

            foreach (var periodo in periodos)
            {
                pickerPeriodo.Items.Add(periodo.CodigoPeriodoLetivo);
                ListaPeriodo.Add(periodo);
            }

            pickerPeriodo.SelectedIndex = -1;
        }

        private void selectedValuePicker(object sender, EventArgs e)
        {
            var selectedValue = pickerPeriodo.Items[pickerPeriodo.SelectedIndex];
            App.PeriodoAtual = selectedValue;
        }

        private async void btnDeslogar(object sender, EventArgs e)
        {
            App.isLogged = false;
            App.MasterDetail.IsPresented = false;
            await Navigation.PushModalAsync(new NavigationPage(new Autenticacao()));
        }
    }
}
