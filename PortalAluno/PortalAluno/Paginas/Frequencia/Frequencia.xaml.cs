﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAluno.Controller;
using Xamarin.Forms;
using System.Diagnostics;

namespace PortalAluno.Paginas.Frequencia
{
    public partial class Frequencia : ContentPage
    {
        private FrequenciaDAL frequenciaDAL = new FrequenciaDAL();
        private FrequenciaController frequenciaController = new FrequenciaController();

        public Frequencia()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            frequenciaController.GravarNomeDisciplina();
            lvFrequencia.ItemsSource = frequenciaDAL.GetAll();
        }

        private void btnAtualizar(object sender, EventArgs args)
        {
            
        }
    }
}
