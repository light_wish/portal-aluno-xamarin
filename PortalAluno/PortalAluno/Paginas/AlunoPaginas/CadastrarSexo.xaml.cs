﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlunoPaginas
{
    public partial class CadastrarSexo : ContentPage
    {
        private SexoDAL sexoDAL = new SexoDAL();
        private Sexo sexo;

        public CadastrarSexo()
        {
            InitializeComponent();
            InicializarTela();
        }

        private void InicializarTela()
        {
            sexo = new Sexo();
            sexo.IdSexo = 0;
            sexo.Descricao = "";
            txtDescricao.Text = sexo.Descricao;
            RecarregarDados();
        }

        private void RecarregarDados()
        {
            listSexo.ItemsSource = sexoDAL.Todos();
        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Sexo;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.Descricao.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                sexoDAL.DeleteById((int)item.IdSexo);
                RecarregarDados();
            }
        }

        public void BtnGravarClick(object sender, EventArgs args)
        {
            if (txtDescricao.Text.Trim() == string.Empty)
            {
                this.DisplayAlert("Erro", "Preencha o campo de descricao", "Ok");
            }

            else
            {
                sexo.Descricao = txtDescricao.Text.Trim();
                sexoDAL.Add(sexo);
                InicializarTela();
            }
        }
    }
}
