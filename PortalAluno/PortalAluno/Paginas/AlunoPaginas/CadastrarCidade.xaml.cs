﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlunoPaginas
{
    public partial class CadastrarCidade : ContentPage
    {
        private CidadeDAL cidadeDAL = new CidadeDAL();
        private Cidade cidade;
        private List<Estado> ListaEstado;

        public CadastrarCidade()
        {
            InitializeComponent();
            InicializarTela();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            listCidades.ItemsSource = cidadeDAL.Todos();
        }

        private void InicializarTela()
        {
            cidade = new Cidade();

            cidade.IdCidade = 0;
            pickerEstado.SelectedIndex = -1;

            cidade.Nome = "";
            txtnome.Text = cidade.Nome;
            cidade.Rua = "";
            txtrua.Text = cidade.Rua;
            cidade.Bairro = "";
            txtbairro.Text = cidade.Bairro;
            cidade.Numero = "";
            txtnumero.Text = cidade.Numero;

            RecarregarDados();
            CarregarEstados();
        }

        private void RecarregarDados()
        {
            listCidades.ItemsSource = cidadeDAL.Todos();
        }

        private void CarregarEstados()
        {
            ListaEstado = new List<Estado>();
            pickerEstado.Items.Clear();

            EstadoDAL estadoDAL = new EstadoDAL();

            var Estados = estadoDAL.Todos();

            foreach (var estado in Estados)
            {
                pickerEstado.Items.Add(estado.Descricao);
                ListaEstado.Add(estado);
            }

        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Cidade;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.Nome.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                cidadeDAL.DeleteById((int)item.IdCidade);
                RecarregarDados();
            }
        }

        public void BtnGravarClick(object sender, EventArgs e)
        {
            if (txtnome.Text.Trim() == string.Empty || txtrua.Text.Trim() == string.Empty || txtbairro.Text.Trim() == string.Empty || txtnumero.Text.Trim() == string.Empty || pickerEstado.SelectedIndex == -1)
            {
                this.DisplayAlert("Erro", "Você precisa informar os dados da cidade.", "Ok");
                return;
            }

            var estado = ListaEstado[pickerEstado.SelectedIndex];

            cidade.Nome = txtnome.Text.Trim();
            cidade.IdEstado = Convert.ToInt32(estado.IdEstado);
            cidade.Rua = txtnome.Text.Trim();
            cidade.Bairro = txtnome.Text.Trim();
            cidade.Numero = txtnome.Text.Trim();

            cidadeDAL.Add(cidade);
            InicializarTela();
        }
    }
}
