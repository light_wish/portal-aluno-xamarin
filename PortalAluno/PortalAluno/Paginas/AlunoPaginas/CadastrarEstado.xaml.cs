﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlunoPaginas
{
    public partial class CadastrarEstado : ContentPage
    {
        private EstadoDAL estadoDAL = new EstadoDAL();
        private Estado estado;

        public CadastrarEstado()
        {
            InitializeComponent();
            InicializarTela();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            RecarregarDados();
        }

        private void InicializarTela()
        {
            estado = new Estado();
            estado.IdEstado = 0;
            estado.Descricao = "";
            txtDescricao.Text = estado.Descricao;
            RecarregarDados();
        }

        private void RecarregarDados()
        {
            listEstado.ItemsSource = estadoDAL.Todos();
        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Estado;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.Descricao.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                estadoDAL.DeleteById((int)item.IdEstado);
                RecarregarDados();
            }
        }

        public void BtnGravarClick(object sender, EventArgs args)
        {
            if (txtDescricao.Text.Trim() == string.Empty)
            {
                this.DisplayAlert("Erro", "Preencha o campo de descricao", "Ok");
            }

            else
            {
                estado.Descricao = txtDescricao.Text.Trim();
                estadoDAL.Add(estado);
                InicializarTela();
            }
        }
    }
}
