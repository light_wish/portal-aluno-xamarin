﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlunoPaginas
{
    public partial class CadastrarAluno : ContentPage
    {
        private AlunoDAL alunoDAL = new AlunoDAL();
        private Aluno aluno;
        private List<Sexo> ListaSexo;
        private List<Cidade> ListaCidade;

        public CadastrarAluno()
        {
            InitializeComponent();
            InicializarTela();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            listAlunos.ItemsSource = alunoDAL.Todos();
        }

        private void InicializarTela()
        {
            aluno = new Aluno();

            pickerSexo.SelectedIndex = -1;
            pickerCidade.SelectedIndex = -1;

            aluno.Nome = "";
            txtnome.Text = aluno.Nome;
            aluno.Matricula = 0;
            txtmatricula.Text = aluno.Matricula.ToString();

            RecarregarDados();
            CarregarSexo();
            CarregarCidade();
        }

        private void RecarregarDados()
        {
            listAlunos.ItemsSource = alunoDAL.Todos();
        }

        private void CarregarSexo()
        {
            ListaSexo = new List<Sexo>();
            pickerSexo.Items.Clear();

            SexoDAL sexoDAL = new SexoDAL();

            var Sexos = sexoDAL.Todos();

            foreach (var sexo in Sexos)
            {
                pickerSexo.Items.Add(sexo.Descricao);
                ListaSexo.Add(sexo);
            }

        }

        private void CarregarCidade()
        {
            ListaCidade = new List<Cidade>();
            pickerCidade.Items.Clear();

            CidadeDAL cidadeDAL = new CidadeDAL();

            var Cidades = cidadeDAL.Todos();

            foreach (var cidade in Cidades)
            {
                pickerCidade.Items.Add(cidade.Nome);
                ListaCidade.Add(cidade);
            }

        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Aluno;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.Nome.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                alunoDAL.DeleteById((int)item.Matricula);
                RecarregarDados();
            }
        }

        public void BtnGravarClick(object sender, EventArgs e)
        {
            if (txtnome.Text.Trim() == string.Empty || txtmatricula.Text.Trim() == string.Empty || pickerSexo.SelectedIndex == -1 || pickerCidade.SelectedIndex == -1)
            {
                this.DisplayAlert("Erro", "Você precisa informar os dados do aluno.", "Ok");
                return;
            }

            var sexo = ListaSexo[pickerSexo.SelectedIndex];
            var cidade = ListaCidade[pickerCidade.SelectedIndex];

            aluno.Nome = txtnome.Text.Trim();
            aluno.IdSexo = Convert.ToInt32(sexo.IdSexo);
            aluno.IdCidade = Convert.ToInt32(cidade.IdCidade);

            alunoDAL.Add(aluno);
            InicializarTela();
        }
    }
}
