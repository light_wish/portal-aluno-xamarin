﻿using PortalAluno.Dal;
using PortalAluno.Model;
using PortalAlunoApplication.Application;
using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AutenticacaoPaginas
{
    public partial class Autenticacao : ContentPage
    {
        public Autenticacao()
        {
            InitializeComponent();
        }

        private void CriarTabelas()
        {
            BancoDeDadosDAL bancoDeDados = new BancoDeDadosDAL();
            bancoDeDados.CriarTabelas();
        }

        private void CriarUsuario(string Nome)
        {
            MBUsuarioDAL mbUsuarioDAL = new MBUsuarioDAL();

            MBUsuario mbUsuario = mbUsuarioDAL.Get();
            if (mbUsuario == null)
            {
                mbUsuario = new MBUsuario();
                mbUsuario.Identificacao = txtUsuario.Text;
                mbUsuario.Senha = txtSenha.Text;
                mbUsuario.Nome = Nome;
                mbUsuarioDAL.Add(mbUsuario);
            }
            else
            {
                mbUsuario.Identificacao = txtUsuario.Text;
                mbUsuario.Senha = txtSenha.Text;
                mbUsuario.Nome = Nome;
                mbUsuarioDAL.Update(mbUsuario);
            }
        }

        private async void BtnAutenticar(object sender, EventArgs args)
        {

            if (lblMensagem.Text != "")
                return;

            Usuario usuario = new Usuario();

            AutenticarApplication autApp = new AutenticarApplication();


            lblMensagem.Text = "Aguarde validando usuário";
            await Task.Delay(50);

            PortalAlunoApplication.Apoio.DadosProxy.UsarProxy = App.UsarProxy;
            PortalAlunoApplication.Apoio.DadosProxy.URLProxy = "proxy.unipam.edu.br:3128";
            PortalAlunoApplication.Apoio.DadosProxy.UsuarioProxy = App.UserProxy;
            PortalAlunoApplication.Apoio.DadosProxy.SenhaProxy = App.PassProxy;

            usuario = autApp.Autenticar(txtUsuario.Text, txtSenha.Text);
            
            if (usuario.Autenticado == "S")
            {
                CriarTabelas();
                CriarUsuario(usuario.Nome);
                App.isLogged = true;
                await Navigation.PopModalAsync();
                lblMensagem.Text = "";
            }
            else
            {
                lblMensagem.Text = "";
                await DisplayAlert("Erro", usuario.Mensagem, "OK");
            }            
        }
    }
}
