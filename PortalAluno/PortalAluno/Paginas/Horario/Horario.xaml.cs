﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;
using PortalAluno.Controller;

namespace PortalAluno.Paginas.Horario
{
    public partial class Horario : ContentPage
    {
        private MBHorarioDAL horariosDAL = new MBHorarioDAL();
        HorarioController horarioController = new HorarioController();

        public Horario()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            horarioController.BuscarHorarios(App.PeriodoAtual);
            lvHorario.ItemsSource = horariosDAL.GetAll();
        }

        private void btnHorario(object sender, EventArgs args)
        {
            horarioController.BuscarHorarios(App.PeriodoAtual);
            lvHorario.ItemsSource = horariosDAL.GetAll();
        }
    }
}
