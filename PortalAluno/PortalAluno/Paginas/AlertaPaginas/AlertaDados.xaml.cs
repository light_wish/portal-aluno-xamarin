﻿using PortalAluno.Dal;
using PortalAluno.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlertaPaginas
{
    public partial class AlertaDados : ContentPage
    {

        private AlertaDAL dalAlerta = new AlertaDAL();
        private Alerta Alerta;
        private List<SituacaoAlerta> ListaSituacaoAlerta;
        private string Origem;

        public AlertaDados()
        {
            InitializeComponent();
            Origem = "I";
            InicializarAlerta();

        }
        public AlertaDados(Alerta alerta)
        {
            InitializeComponent();
            Origem = "A";

            CarregaSituacaoAlerta();

            Alerta = alerta;
            PrepararTela();
        }

        private void InicializarAlerta()
        {
            CarregaSituacaoAlerta();
            pickerSituacao.SelectedIndex = -1;
            Alerta = new Alerta();
            Alerta.IdAlerta = 0;
            Alerta.DescricaoAlerta = "";
            Alerta.Data = DateTime.Now.Date;
            Alerta.Hora = Convert.ToDateTime(DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second).ToString();
            Alerta.Valor = 0;
            PrepararTela();
        }

        private void CarregaSituacaoAlerta()
        {

            ListaSituacaoAlerta = new List<SituacaoAlerta>();
            pickerSituacao.Items.Clear();

            SituacaoAlertaDAL situacaoAlertaDAL = new SituacaoAlertaDAL();

            var situacaoAlertas = situacaoAlertaDAL.GetAll();

            foreach (var situacaoAlerta in situacaoAlertas)
            {
                pickerSituacao.Items.Add(situacaoAlerta.Descricao);
                ListaSituacaoAlerta.Add(situacaoAlerta);
            }

        }

        private void PrepararTela()
        {
            txtdescricaoAlerta.Text = Alerta.DescricaoAlerta;
            txtdata.Date = Alerta.Data;
            txtHora.Time = Convert.ToDateTime(Alerta.Hora).TimeOfDay;
            txtValor.Text = Alerta.Valor.ToString();


            int i = 0;
            string situacaoEncontrada = "N";
            foreach (var situacaoAlerta in ListaSituacaoAlerta)
            {

                if (situacaoAlerta.IdSituacao == Alerta.IdSituacao)
                {
                    situacaoEncontrada = "S";
                    break;
                }
                else
                    i = i + 1;
            }
            if (situacaoEncontrada == "N")
                i = -1;

            pickerSituacao.SelectedIndex = i;

        }

        public void BtnGravarClick(object sender, EventArgs e)
        {
            if (txtdescricaoAlerta.Text.Trim() == string.Empty)
            {
                this.DisplayAlert("Erro", "Você precisa informar a descrição do Alerta.", "Ok");
                return;
            }
            if (pickerSituacao.SelectedIndex == -1)
            {
                this.DisplayAlert("Erro", "Você precisa selecionar uma situação para o Alerta.", "Ok");
                return;
            }
            if (txtValor.Text.Trim() == "")
                txtValor.Text = "0";

            var situacaoAlerta = ListaSituacaoAlerta[pickerSituacao.SelectedIndex];

            Alerta.DescricaoAlerta = txtdescricaoAlerta.Text.Trim();
            Alerta.IdSituacao = Convert.ToInt32(situacaoAlerta.IdSituacao);
            Alerta.Data = txtdata.Date;
            Alerta.Hora = txtHora.Time.ToString();
            Alerta.Valor = Convert.ToDouble(txtValor.Text);


            if (Alerta.IdAlerta == 0)
            {
                dalAlerta.Add(Alerta);
                InicializarAlerta();
            }
            else
            {
                dalAlerta.Update(Alerta);
                if (Origem == "A")
                    Navigation.PopModalAsync(true);
                else
                    InicializarAlerta();
            }

        }
    }
}
