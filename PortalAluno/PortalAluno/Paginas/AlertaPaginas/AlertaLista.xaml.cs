﻿using PortalAluno.Dal;
using PortalAluno.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.AlertaPaginas
{
    public partial class AlertaLista : ContentPage
    {
        private AlertaDAL dalAlerta = new AlertaDAL();
        public AlertaLista()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CarregarLista();
        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Alerta;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.DescricaoAlerta.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                dalAlerta.DeleteById((int)item.IdAlerta);
                CarregarLista();
            }
        }

        public async void OnAlterarClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as Alerta;
            await Navigation.PushModalAsync(new AlertaDados(item));
        }

        private void CarregarLista()
        {

            var lista = dalAlerta.GetAll();
            lvAlerta.ItemsSource = lista;
        }
    }
}
