﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalAluno.Dal;
using PortalAluno.Model;

using Xamarin.Forms;
using PortalAluno.Controller;

namespace PortalAluno.Paginas.Atividades
{
    public partial class Atividades : ContentPage
    {
        private MBAtividadeDAL atividadesDAL = new MBAtividadeDAL();

        public Atividades()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lvAtividades.ItemsSource = atividadesDAL.GetAll();
        }

        private void btnAtualizar(object sender, EventArgs args)
        {
            lvAtividades.ItemsSource = atividadesDAL.GetAll();
        }

        private void btnFiltroParticipacao(object sender, EventArgs args)
        {
            lvAtividades.ItemsSource = atividadesDAL.GetByParticipacao();
        }

        private void btnFiltroData(object sender, EventArgs args)
        {
            lvAtividades.ItemsSource = atividadesDAL.GetByData();
        }
    }
}
