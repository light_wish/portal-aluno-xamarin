﻿using PortalAluno.Paginas.SituacaoPaginas;
using PortalAlunoModel.Modelo;
using PortalAluno.Paginas.AlertaPaginas;
using PortalAluno.Paginas.AlunoPaginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using PortalAluno.Controller;

namespace PortalAluno.Paginas
{
    public partial class Master : ContentPage
    {
        public Master()
        {
            InitializeComponent();
            //Usuario usuario = (Usuario)(Application.Current.Properties["Usuario"]);
            //Title = "Portal do Aluno " + usuario.Nome;
        }
        public Master(Usuario usuario)
        {
            InitializeComponent();
            Title = "Portal do Aluno " + usuario.Nome;
        }
        private async void btnSituacao(object sender, EventArgs args)
        {
            //await DisplayAlert("Alert", "Situacao", "OK"); 
             await Navigation.PushAsync(new SituacaoComando());

        }
        private async void btnEvento(object sender, EventArgs args)
        {
             //await DisplayAlert("Alert", "Evento", "OK");
             await Navigation.PushModalAsync(new AlertaComando());
        }

        private async void btnAluno(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new AlunoComando());
        }

        private async void btnFechar(object sender, EventArgs args)
        {
            await Navigation.PopAsync(true);
        }

        private async void btnSincronizar(object sender, EventArgs args)
        {
            SincronizarController sincronizar = new SincronizarController();
            lblMensagem.Text = "Aguarde sincronizando dados";
            await Task.Delay(50);
            var mensagem = sincronizar.SincronizarDados("2/2016");
            if(mensagem.Trim() != "")
            {
                await DisplayAlert("Erro", "Erro na sincronizacao " + mensagem, "OK");
            }
            else
            {
                lblMensagem.Text = "Dados sincronizados com sucesso";
            }
        }


    }
}
