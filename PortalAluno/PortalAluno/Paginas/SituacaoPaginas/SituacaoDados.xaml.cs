﻿using PortalAluno.Dal;
using PortalAluno.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.SituacaoPaginas
{
    public partial class SituacaoDados : ContentPage
    {

        private SituacaoAlertaDAL dalSituacaoAlerta = new SituacaoAlertaDAL();
        private SituacaoAlerta SituacaoAlerta;

        public SituacaoDados()
        {
            InitializeComponent();
            InicializarSituacaoAlerta();
        }
        public SituacaoDados(SituacaoAlerta situacaoAlerta)
        {
            InitializeComponent();
            SituacaoAlerta = situacaoAlerta;
            PrepararTela();
        }
        private void InicializarSituacaoAlerta()
        {
            SituacaoAlerta = new SituacaoAlerta();
            SituacaoAlerta.IdSituacao = 0;
            SituacaoAlerta.Descricao = "";
            PrepararTela();

        }
        private void PrepararTela()
        {
            txtdescricao.Text = SituacaoAlerta.Descricao;
        }
        public void BtnGravarClick(object sender, EventArgs e)
        {
            if (txtdescricao.Text.Trim() == string.Empty)
            {
                this.DisplayAlert("Erro", "Você precisa informar a descrição da situação do Alerta.", "Ok");
            }
            else
            {
                SituacaoAlerta.Descricao = txtdescricao.Text.Trim();
                if (SituacaoAlerta.IdSituacao == 0)
                {
                    dalSituacaoAlerta.Add(SituacaoAlerta);
                    InicializarSituacaoAlerta();
                }
                else
                {
                    dalSituacaoAlerta.Update(SituacaoAlerta);
                    Navigation.PopModalAsync(true);
                }
            }
        }
    }
}
