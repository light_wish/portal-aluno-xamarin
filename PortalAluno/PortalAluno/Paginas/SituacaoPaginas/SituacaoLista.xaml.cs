﻿using PortalAluno.Dal;
using PortalAluno.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PortalAluno.Paginas.SituacaoPaginas
{
    public partial class SituacaoLista : ContentPage
    {
        private SituacaoAlertaDAL dalSituacaoAlerta = new SituacaoAlertaDAL();
        public SituacaoLista()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lvSituacao.ItemsSource = dalSituacaoAlerta.GetAll();
        }

        public async void OnRemoverClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as SituacaoAlerta;
            var opcao = await DisplayAlert("Confirmação de exclusão", "Confirma excluir o item " + item.Descricao.ToUpper() + "?", "Sim", "Não");
            if (opcao)
            {
                dalSituacaoAlerta.DeleteById((int)item.IdSituacao);
                lvSituacao.ItemsSource = dalSituacaoAlerta.GetAll();
            }
        }

        public async void OnAlterarClick(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var item = mi.CommandParameter as SituacaoAlerta;
            await Navigation.PushModalAsync(new SituacaoDados(item));
        }
    }
}
