using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PortalAluno.Droid;
 
using System.IO;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
using PortalAluno.Infraestrutura;

[assembly: Xamarin.Forms.Dependency(typeof(ConexaoBancoDeDados))]
namespace PortalAluno.Droid
{
    public class ConexaoBancoDeDados : IBancoDeDados
    {
        public SQLiteConnection DbConnection()
        {
            var dbName = "PortalAluno.db3";
            string documentsFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsFolder, dbName);
            var platform = new SQLitePlatformAndroid();
            return new SQLiteConnection(platform, path, false);
        }
    }
}