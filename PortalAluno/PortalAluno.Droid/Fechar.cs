using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PortalAluno.Infraestrutura;
using PortalAluno.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(Fechar))]
namespace PortalAluno.Droid
{
    public class Fechar : IFecharSistema
    {
        public void FecharAplicativo()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
    }
}