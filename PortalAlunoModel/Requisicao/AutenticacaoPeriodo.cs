﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Requisicao
{
    public class AutenticacaoPeriodo
    {
        public Autenticacao Autenticacao { get; set; }
        public string PeriodoLetivo { get; set; }
        public AutenticacaoPeriodo()
        {
            Autenticacao = new Autenticacao();
            PeriodoLetivo = "";
        }
    }
}
