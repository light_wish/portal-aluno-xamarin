﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Requisicao
{
    public class Autenticacao
    {
        public string Matricula { get; set; }
        public string Senha { get; set; }
        public Autenticacao()
        {
            Matricula = "";
            Senha = "";
        }
    }
}
