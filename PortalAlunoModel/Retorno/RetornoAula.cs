﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoAula
    {
        public Retorno Retorno { get; set; }
        public List<Aula> Aulas { get; set; }
        public RetornoAula()
        {
            Aulas = new List<Aula>();
            Retorno = new Retorno();
        }
    }
}
