﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoHorario

    {
        public Retorno Retorno { get; set; }
        public List<Horario> Horarios { get; set; }

        public RetornoHorario()
        {
            Retorno = new Retorno();
            Horarios = new List<Horario>();
        }
    }
}
