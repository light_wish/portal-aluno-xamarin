﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoDisciplina
    {
        public Retorno Retorno { get; set; }
        public List<Disciplina> Disciplinas { get; set; }
        public RetornoDisciplina()
        {
            Retorno = new Retorno();
            Disciplinas = new List<Disciplina>();
        }
    }
}
