﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoTodos
    {
        public Retorno Retorno { get; set; }
        public List<Atividade> Atividades { get; set; }
        public List<Aula> Aulas { get; set; }
        public List<Disciplina> Disciplinas { get; set; }
        public List<Nota> Notas { get; set; }
        public RetornoTodos()
        {
            Atividades = new List<Atividade>();
            Retorno = new Retorno();
            Aulas = new List<Aula>();
            Disciplinas = new List<Disciplina>();
            Notas = new List<Nota>();
        }
    }
}
