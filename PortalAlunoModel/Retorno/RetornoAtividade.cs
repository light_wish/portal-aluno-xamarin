﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoAtividade
    {
        public Retorno Retorno { get; set; }
        public List<Atividade> Atividades { get; set; }
        public RetornoAtividade()
        {
            Atividades = new List<Atividade>();
            Retorno = new Retorno();
        }
    }
}
