﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoPeriodoLetivo
    {
        public Retorno Retorno { get; set; }
        public List<PeriodoLetivo> PeriodosLetivos { get; set; }

        public RetornoPeriodoLetivo()
        {
            Retorno = new Retorno();
            PeriodosLetivos = new List<PeriodoLetivo>();
        }
    }
}
