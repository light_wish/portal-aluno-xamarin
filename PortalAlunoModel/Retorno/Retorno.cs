﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class Retorno
    {
        public string Matricula { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public string Mensagem { get; set; }
        public string Autenticado { get; set; }

        public Retorno()
        {
            Matricula = "";
            Senha = "";
            Nome = "";
            Mensagem = "";
            Autenticado = "N";
        }
    }
}
