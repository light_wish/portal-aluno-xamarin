﻿using PortalAlunoModel.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Retorno
{
    public class RetornoNota
    {
        public Retorno Retorno { get; set; }
        public List<Nota> Notas { get; set; }
        public RetornoNota()
        {
            Notas = new List<Nota>();
            Retorno = new Retorno();
        }
    }
}
