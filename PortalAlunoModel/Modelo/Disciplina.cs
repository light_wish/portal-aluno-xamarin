﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class Disciplina
    {
        public int CodigoDisciplina { get; set; }
        public string NomeDisciplina { get; set; }
        public string CodigoTurma { get; set; }
    }
}
