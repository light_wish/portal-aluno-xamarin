﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class Aula
    {
        public int CodigoAula { get; set; }
        public string DataAula { get; set; }
        public int QuantidadeAulas { get; set; }
        public string Conteudo { get; set; }
        public string IndicadorDeFalta { get; set; }
        public int CodigoDisciplina { get; set; }
    }
}
