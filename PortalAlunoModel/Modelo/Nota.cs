﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class Nota
    {
        public int CodigoNota { get; set; }
        public string NomeAvaliacao { get; set; }
        public string ValorAluno { get; set; }
        public string ValorAvaliacao { get; set; }
        public int CodigoDisciplina { get; set; }
    }
}
