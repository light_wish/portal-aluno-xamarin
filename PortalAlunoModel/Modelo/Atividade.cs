﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class Atividade
    {
        public int CodigoAtividade { get; set; }
        public string DescricaoAtividade { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
        public string HoraFim { get; set; }
        public string IndicadorParticipacao { get; set; }
        public int CodigoDisciplina { get; set; }
    }
}
