﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class PeriodoLetivo
    {

        public string CodigoPeriodoLetivo { get; set; }
        public string DescricaoPeriodoLetivo { get; set; }

        public PeriodoLetivo()
        {
            CodigoPeriodoLetivo = "";
            DescricaoPeriodoLetivo = "";
        }
    }
}
