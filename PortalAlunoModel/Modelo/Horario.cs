﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalAlunoModel.Modelo
{
    public class Horario
    {
        public string CodigoPeriodoLetivo { get; set; }
        public int NumeroDiaDaSemana { get; set; }
        public string DescricaoDiaSemana { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFim { get; set; }
        public string NomeDisciplina { get; set; }
        public Horario()
        {
            CodigoPeriodoLetivo = "";
            NumeroDiaDaSemana = 0;
            DescricaoDiaSemana = "";
            HoraInicio = "";
            HoraFim = "";
            NomeDisciplina = "";

        }
    }
}
